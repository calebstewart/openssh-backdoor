# OpenSSH Backdoor Script

This script takes an OpenSSH Server (`sshd`) binary and patches it such that any given Private Key will authenticate to any user. This is done by patching the `user_key_allowed` function to always return success. The script needs debugging symbols to find the `user_key_allowed` function. If your `sshd` binary is stripped, pass a version with debugging symbols as the second parameter to this script.

In some cases, `pwntools` is unable to load ELF files with only debug symbols. In these cases, you can rebuild the debugging version of your sshd given the debugging symbols and the stripped binary using the `elfutils` package:

```bash
eu-unstrip -o sshd-dbg ./sshd ./sshd.debug
```

If you go this route, I would recommend invoking the backdoor script with both the stripped and unstripped binaries in order to ensure your backdoored version is also stripped (closely matching the installed version):

```
./backdoor.py -o sshd.backdoor ./sshd ./sshd-dbg
```

After patching, you can overwrite your original `sshd` binary with the backdoored version. You will have to stop the SSH server first. After restarting the server, any Private Key will authenticate to the service as any user.
